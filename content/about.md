---
title: "About"
layout: "about"
---

Hi! My name is Andrei, I'm an Android Developer by craft. I write code since 2010, touched various tech stacks during this time, but stayed on Android. I participate in open source projects, and maintain some of them myself.

In this blog, I will focus on technical articles about stuff I encountered in my works. Hope it will be useful to people.
